using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Unity.MLAgents;
using Unity.MLAgents.Sensors;

public class player : Agent
{
    [SerializeField] Animator animatorController;
    Rigidbody rb;
    bool m_jump;
    [SerializeField] float intensity;
    [SerializeField] float speed;
    [SerializeField] GameObject rayPerception_far;
    [SerializeField] GameObject rayPerception_near;
    RayPerceptionSensorComponent3D rayPerception_far_comp, rayPerception_near_comp;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        rayPerception_far_comp = rayPerception_far.GetComponent<RayPerceptionSensorComponent3D>();
        rayPerception_near_comp = rayPerception_near.GetComponent<RayPerceptionSensorComponent3D>();
    }

    private void Update()
    {
        animatorController.SetBool("jump", m_jump);
        AddReward(1.0f);

        if (Input.GetKeyDown(KeyCode.Space) & !m_jump)
        {
            m_jump = true;
            rb.AddForce(Vector3.up * intensity, ForceMode.Impulse);
        }
    }
    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.A))
        {
            rb.MovePosition(transform.position + Vector3.left * Time.deltaTime * speed);
        }

        if (Input.GetKey(KeyCode.D))
        {
            rb.MovePosition(transform.position + Vector3.right * Time.deltaTime * speed);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Ground"))
        {
            if (m_jump == true)
            {
                m_jump = false;
            }
        }

        if (collision.gameObject.layer == LayerMask.NameToLayer("Obstacle"))
        {
            SetReward(-5.0f);
            EndEpisode();
            SceneManager.LoadScene(0);
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag == "side")
        {
            AddReward(-0.5f);
        }

        if (collision.gameObject.layer == LayerMask.NameToLayer("Ground"))
        {
            AddReward(0.1f);
        }
    }

    public override void CollectObservations(VectorSensor sensor)
    {
        sensor.AddObservation(transform.position.x);
        sensor.AddObservation(transform.position.y);
        sensor.AddObservation(m_jump ? 1f : -1f);
        sensor.AddObservation(rayPerception_far_comp);
        sensor.AddObservation(rayPerception_near_comp);
    }

    public override void OnActionReceived(float[] vectorAction)
    {
        int moveLeft = Mathf.FloorToInt(vectorAction[0]);
        int moveRight = Mathf.FloorToInt(vectorAction[1]);
        int jump = Mathf.FloorToInt(vectorAction[2]);
        if (moveLeft == 1)
        {
            rb.MovePosition(transform.position + Vector3.left * Time.deltaTime * speed);
        }
        if (moveRight == 1)
        {
            rb.MovePosition(transform.position + Vector3.right * Time.deltaTime * speed);
        }
        if (jump == 1 && !m_jump)
        {
            m_jump = true;
            rb.AddForce(Vector3.up * intensity, ForceMode.Impulse);
            AddReward(-0.1f);
        }
    }
}
