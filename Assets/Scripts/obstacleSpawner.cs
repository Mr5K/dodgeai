using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class obstacleSpawner : MonoBehaviour
{
    [SerializeField] List<GameObject> obstacles = new List<GameObject>();
    [SerializeField] Text highscoreText;
    [SerializeField] Text scoreText;
    int score = 0;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(spawnObstacle());
        StartCoroutine(addScore());
        highscoreText.text = "Highscore: " + PlayerPrefs.GetInt("hs").ToString();

    }


    IEnumerator spawnObstacle()
    {
        float r = Random.Range(.8f, 1.5f);
        spawnO();
        yield return new WaitForSeconds(r);
        StartCoroutine(spawnObstacle());
    }

    void spawnO()
    {
        int r = Random.Range(0, 100);
        if (r <= 50)
        {
            Instantiate(obstacles[0], new Vector3(0, 0, 65), Quaternion.identity);
        }
        else
        {
            Instantiate(obstacles[1], new Vector3(0, 0, 65), Quaternion.identity);
        }
    }

    IEnumerator addScore()
    {
        score++;
        scoreText.text = "Score: " + score.ToString();
        yield return new WaitForSeconds(1);
        StartCoroutine(addScore());
    }

    private void OnDestroy()
    {
        if (score > PlayerPrefs.GetInt("hs"))
        {
            PlayerPrefs.SetInt("hs", score);
        }
    }
}
