using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class obstacleBehav : MonoBehaviour
{
    [SerializeField] int speed;
    [SerializeField] bool saw;
    [SerializeField] bool crate;
    [SerializeField] float sawSpeed;
    bool left;
    Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        if (saw || crate)
        {
            float r = Random.Range(-5f, 6f);
            transform.position = new Vector3(r, 0, 65);
            if (Random.value >= 0.5)
                left = true;
            else
                left = false;
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        if (saw)
        {
            if (left)
            {
                if (transform.position.x >= -4f)
                {
                    rb.MovePosition(transform.position + new Vector3(-sawSpeed, 0, -1) * Time.deltaTime * speed);
                }
                else
                {
                    left = false;
                }
            }
            else
            {
                if (transform.position.x <= 4f)
                {
                    rb.MovePosition(transform.position + new Vector3(sawSpeed, 0, -1) * Time.deltaTime * speed);
                }
                else
                {
                    left = true;
                }
            }
        }
        else
            rb.MovePosition(transform.position + new Vector3(0, 0, -1) * Time.deltaTime * speed);
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
