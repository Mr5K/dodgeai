# dodgeAI
This is an reinforcement learning project that implements mlAgents in an unity environment. The agent moves straight forward in the environment and must dodge the appearing obstacles by moving to the left, right or  jump above it.

![example run of the trained agent](result/Runner10.mp4)

## Dependencies
To install the depencencies simply run
```
pip install -r requirements.txt
```